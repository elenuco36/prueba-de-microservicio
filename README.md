# Prueba de Microservicio

Crear un servicio web en Node.js que exponga un punto de acceso (ENDPOINT) POST,
el cual reciba dos cadenas de texto, luego ejecutar un script en Python o
comunicarse con un servicio en Pytho que retorne el porcentaje de coincidencia
entre las dos cadenas y cuantos caracteres coinciden en la misma posicion a
Node.js, finalmente retornar la respuesta.

```mermaid
sequenceDiagram
    participant Client
    participant Node as Node.js service
    participant Python as Python service

    Client->>+Node: POST :1337/ string1=hello string2=hi
    Node->>+Python: GET :3000/levenshtein?string1=hello&string2=hi
    Python->>Python: levenshtein_distance("hello", "hi")
    Python->>-Node: { "distance": 4 }
    Node->>-Client: { "coincidence": 1, "percentage": 0.2 }
```
