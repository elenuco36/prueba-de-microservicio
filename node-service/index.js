const { STATUS_CODES } = require('http');
const express = require('express');

const { requestCalc } = require('./client');

/**
 * The payload of the request
 *
 * @typedef {Object} Payload
 * @property {string} string1 First string
 * @property {string} string2 Second string
 */

/**
 * Listen for incoming requests
 *
 * @param {express.Request} req Request
 * @param {express.Response} res Response
 */
async function requestListener(req, res) {
  const errors = [];

  if (!req.body?.string1) {
    errors.push('The string1 is required');
  }
  if (!req.body?.string2) {
    errors.push('The string2 is required');
  }
  if (errors.length > 0) {
    return res.status(422).json({
      statusCode: 422,
      message: STATUS_CODES[422],
      errors,
    });
  }

  /**
   * @type {Payload}
   */
  const { string1, string2 } = req.body;

  try {
    const result = await requestCalc(string1, string2);
    const length = Math.max(string1.length, string2.length);
    const percentage = (length - result.distance) / length;
    const coincidence = length - result.distance;

    res.json({ coincidence, percentage });
  } catch (error) {
    console.error(error);
    res.status(502).json({ statusCode: 502, message: STATUS_CODES[502] });
  }
}

const app = express();
const port = parseInt(process.env.PORT, 10) || 1337;

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.post('/', requestListener);

app
  .listen(port, () => {
    console.info(`Node.js service listening on port ${port}`);
  })
  .on('error', console.error);
