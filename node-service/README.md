# Node.js service

## Requirements

- Node.js >= 14.10.0

## Instructions

Install the dependencies

```bash
npm install
```

Run the server in watch mode

```bash
npm run dev
```

## Documentation API

**URL:** /

**Method:** POST

**Description:** Find the coincidence between two strings

**Body:**

| Name    | Type   | Required | Description       |
| ------- | ------ | -------- | ----------------- |
| string1 | String | Yes      | The first string  |
| string2 | String | Yes      | The second string |

**Responses:**

_200 OK_

| Name        | Type    | Required | Description                                            |
| ----------- | ------- | -------- | ------------------------------------------------------ |
| coincidence | Integer | Yes      | The number of characters that are in the same position |
| percentage  | Float   | Yes      | The percentage of coincidence                          |

Example

```json
{
  "coincidence": 1,
  "percentage": 0.2
}
```

_422 Unprocessable Entity_

Example

```json
{
  "statusCode": 422,
  "message": "Unprocessable Entity",
  "errors": [
    "The string1 is required",
    "The string2 is required"
  ]
}
```

_502 Bad Gateway_

Example

```json
{
  "statusCode": 502,
  "message": "Bad Gateway"
}
```
